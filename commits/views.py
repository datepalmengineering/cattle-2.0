from commits.models import Commit
from runs.models import Run
from branches.models import Filter, Criterium
from models import Commit
from branches.models import Branch

from rest_framework import generics
from django.contrib.auth.models import User
from rest_framework import permissions
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework import renderers
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render_to_response
import re

ENTRIES_PER_PAGE = 25  # effects paginated views in this file


def commitreport(request, revision):
    commit_list = Commit.objects.filter(revision=int(revision))
    run_list = Run.objects.none()
    if commit_list:
        run_list = Run.objects.filter(commit=commit_list)
    else:
        return HttpResponse("ERROR: Invalid/Non-existent Commit!")

    summary = ""
    overall = "INVL"
    if run_list:
        myPasses = len(run_list.filter(run_results=1)) + \
            len(run_list.filter(run_results=2))
        myFailures = len(run_list.filter(run_results=0)) + \
            len(run_list.filter(run_results=60))
        myRunning = len(run_list.filter(run_results=13))
        if myFailures > 0:
            overall = "FAIL"
        elif myRunning > 0:
            overall = "IPROG"
        elif myPasses > 0:
            overall = "PASS"
        else:
            overall = "INVL"

        summary = str(myPasses) + " Passes, " + str(myFailures) + \
            " Fails, " + str(myRunning) + " In Progress"

    return render_to_response('commits/report.html',
                              {"summary": summary,
                               "overall": overall,
                               "commit": commit_list[0],
                                  "runs": run_list.order_by('-end_time'),
                                  "request": request})


def commitreport_dev(request, revision):
    commit_list = Commit.objects.filter(revision=int(revision))
    run_list = Run.objects.none()
    if commit_list:
        run_list = Run.objects.filter(commit=commit_list)
    else:
        return HttpResponse("ERROR: Invalid/Non-existent Commit!")

    # ############## identify filters ####################
    allFilters = Filter.objects.all()
    filterList = []

    for theFilter in allFilters:
        # check if match
        pattern = re.search(theFilter.regmatch, str(commit_list[0].branch))
        # pattern != None:#pattern.search(commit_list[0].branch.full) is not
        # None:
        if pattern:
            filterList.append(theFilter)

    # ############## gather criteria #####################
    criteriums = Criterium.objects.none()

    for myFilter in filterList:
        criteriums = criteriums | Criterium.objects.filter(filter=myFilter)

    # ############## make builder list ###################

    builderlist = []

    for criteria in criteriums:
        for mybuilder in criteria.builders.all():
            builderlist.append(mybuilder)

    # ################ make overall assessment ###########

    summary = ""
    overall = "INVL"
    if run_list:
        myPasses = len(run_list.filter(run_results=1)) + \
            len(run_list.filter(run_results=2))
        myFailures = len(run_list.filter(run_results=0)) + \
            len(run_list.filter(run_results=60))
        myRunning = len(run_list.filter(run_results=13))
        if myFailures > 0:
            overall = "FAIL"
        elif myRunning > 0:
            overall = "IPROG"
        elif myPasses > 0:
            overall = "PASS"
        else:
            overall = "INVL"

        if criteriums:
            mycriteriastate = "PASS"  # true means passing
            for criteria in criteriums:
                criteriaruns = run_list.filter(
                    builder__in=criteria.builders.all())
                criteriaruns = criteriaruns.filter(
                    suite_file__contains=criteria.suitematch)
                if criteriaruns:
                    if (len(criteriaruns.filter(run_results=0)) +
                            len(criteriaruns.filter(run_results=60))) > 0:
                        mycriteriastate = "FAIL"  # Found failures
                        break  # exit loop to save time
                else:
                    mycriteriastate = "IPROG"  # No tests found

            if mycriteriastate == "FAIL":
                overall = "FAIL"
            elif mycriteriastate == "IPROG" and overall != "IPROG":
                overall = "BLOCKED"
            elif mycriteriastate == "IPROG":
                overall = "IPROG"
            else:
                # catches the case where testing is being run even if criteria
                # has been met.
                if overall != "IPROG":
                    overall = "PASS"

            summary = str(myPasses) + " Passes, " + str(myFailures) + " Fails, " + str(myRunning) + \
                " In Progress <br> <b>Criterium assessment was " + str(mycriteriastate) + "</b>"

        else:
            summary = str(myPasses) + " Passes, " + str(myFailures) + " Fails, " + str(
                myRunning) + " In Progress <br> <b>NO CRITERIUMS FOUND USING BASIC ASSESMENT</b>"
    else:
        if criteriums:
            summary = "<b>No operations have been reported, but according to found criteria automated testing is required (and should already be scheduled) for this commit.</b>"
            overall = "BLOCKED"
        else:
            summary = "<b>No criteria or runs found for this build... It is possible no testing is required for this commit.</b>"
            overall = "INVL"

    return render_to_response('commits/report_dev.html',
                              {"summary": summary,
                               "overall": overall,
                               "commit": commit_list[0],
                                  "runs": run_list.order_by('end_time'),
                                  "request": request,
                                  "filterList": filterList,
                                  "criteriums": criteriums,
                                  "builderlist": builderlist})


def quickassess_revision(revision):
    commit_list = Commit.objects.filter(revision=int(revision))
    run_list = Run.objects.none()
    if commit_list:
        run_list = Run.objects.filter(commit=commit_list)
    else:
        return {"summary": "ERROR: Commit not found!?", "overall": "INVL"}

    # ############## identify filters ####################
    allFilters = Filter.objects.all()
    filterList = []

    for theFilter in allFilters:
        # check if match
        pattern = re.search(theFilter.regmatch, str(commit_list[0].branch))
        # pattern != None:#pattern.search(commit_list[0].branch.full) is not
        # None:
        if pattern:
            filterList.append(theFilter)

    # ############## gather criteria #####################
    criteriums = Criterium.objects.none()

    for myFilter in filterList:
        criteriums = criteriums | Criterium.objects.filter(filter=myFilter)

    # ################ make overall assessment ###########
    summary = ""
    overall = "INVL"
    if run_list:
        myPasses = len(run_list.filter(run_results=1)) + \
            len(run_list.filter(run_results=2))
        myFailures = len(run_list.filter(run_results=0)) + \
            len(run_list.filter(run_results=60))
        myRunning = len(run_list.filter(run_results=13))
        if myFailures > 0:
            overall = "FAIL"
        elif myRunning > 0:
            overall = "IPROG"
        elif myPasses > 0:
            overall = "PASS"
        else:
            overall = "INVL"

        if criteriums:
            mycriteriastate = "PASS"  # true means passing
            for criteria in criteriums:
                criteriaruns = run_list.filter(
                    builder__in=criteria.builders.all())
                criteriaruns = criteriaruns.filter(
                    suite_file__contains=criteria.suitematch)
                if criteriaruns:
                    if (len(criteriaruns.filter(run_results=0)) +
                            len(criteriaruns.filter(run_results=60))) > 0:
                        mycriteriastate = "FAIL"  # Found failures
                        break  # exit loop to save time
                else:
                    mycriteriastate = "IPROG"  # No tests found

            if mycriteriastate == "FAIL":
                overall = "FAIL"
            elif mycriteriastate == "IPROG" and overall != "IPROG":
                overall = "BLOCKED"
            elif mycriteriastate == "IPROG":
                overall = "IPROG"
            else:
                # catches the case where testing is being run even if criteria
                # has been met.
                if overall != "IPROG":
                    overall = "PASS"

            return {
                "summary": str(myPasses) +
                "P " +
                str(myFailures) +
                "F " +
                str(myRunning) +
                "IP",
                "overall": str(overall)}
        else:
            return {
                "summary": str(myPasses) +
                "P " +
                str(myFailures) +
                "F " +
                str(myRunning) +
                "IP",
                "overall": str(overall)}
    else:
        if criteriums:
            return {"summary": "Testing queued...", "overall": "BLOCKED"}

    return {"summary": "No runs or Criteria", "overall": str(overall)}


def list(request, author=None):
    commit_list = Commit.objects.none()
    if author:
        commit_list = Commit.objects.filter(
            author=str(author)).order_by('-revision')
    else:
        commit_list = Commit.objects.all().order_by('-revision')
    paginator = Paginator(commit_list, ENTRIES_PER_PAGE)

    page = request.GET.get('page')
    try:
        commits = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        commits = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        commits = paginator.page(paginator.num_pages)

    for i, s in enumerate(commits):
        myquickassessment = quickassess_revision(commits[i].revision)
        commits[i].overall = myquickassessment["overall"]
        commits[i].summary = myquickassessment["summary"]

    return render_to_response(
        'commits/list.html', {"commits": commits, "request": request})


@api_view(['GET', 'POST'])
def api_root(request, number):
    if request.method == 'POST':
        queryset = Commit.objects.filter(
            revision=int(request.data["revision"]))

        branch_name = request.data["branch"]
        if branch_name.startswith('full/'):
            branch_name = branch_name[len('full/'):]

        mybranch = Branch.objects.filter(full=branch_name)
        if mybranch.exists():
            mybranch = mybranch[0]
        else:
            mybranch = Branch(full=branch_name)
            mybranch.save()

        if queryset.exists():

            queryset.update(
                revision=int(request.data["revision"]),
                branch=mybranch,
                files=str(request.data["files"]),
                author=str(request.data["author"]),
                message=str(request.data["message"])
            )
            queryset[0].save()

            return Response({"response": "Found a match!",
                             "PK": queryset[0].pk,
                             "revision": queryset[0].revision,
                             "branch": queryset[0].branch.full,
                             "author": queryset[0].author,
                             "files": queryset[0].files,
                             "message": queryset[0].message,
                             })
        else:
            queryset = Commit(
                revision=int(request.data["revision"]),
                branch=mybranch,
                files=str(request.data["files"]),
                author=str(request.data["author"]),
                message=str(request.data["message"])
            )
            queryset.save()

            return Response({"response": "No match, new commit created.",
                             "PK": queryset.pk,
                             "revision": queryset.revision,
                             "branch": queryset.branch.full,
                             "author": queryset.author,
                             "files": queryset.files,
                             "message": queryset.message,
                             })

    if number is None or number == "":
        number = -1

    queryset = Commit.objects.filter(revision=int(number))
    if queryset.exists():
        return Response({"response": "Found a match!",
                         "PK": queryset[0].pk,
                         "revision": queryset[0].revision,
                         "message": queryset[0].message,
                         "auther": queryset[0].author,
                         "files": queryset[0].files})
    else:
        return Response(
            {
                "response": "Post the builder address you need to add or search for.",
                "revision": int(number)})
