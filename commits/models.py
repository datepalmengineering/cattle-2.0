from django.db import models
from branches.models import Branch
# Create your models here.


class Commit(models.Model):
    revision = models.IntegerField(primary_key=True)
    branch = models.ForeignKey(Branch, blank=True, null=True)
    message = models.CharField(max_length=1024, blank=True)
    author = models.CharField(max_length=128, blank=True)
    bug = models.CharField(max_length=128, blank=True)
    files = models.CharField(max_length=5000, blank=True)

    def __str__(self):              # __unicode__ on Python 2
        return str(self.revision) + " author => " + \
            str(self.author) + " message => " + str(self.message)
