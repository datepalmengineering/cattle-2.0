from django.conf.urls import url, include
from django.conf.urls import include
from rest_framework.urlpatterns import format_suffix_patterns
from commits import views

# API endpoints
urlpatterns = format_suffix_patterns([
    url(r'^list$', views.list, name='list'),
    url(r'^report/(?P<revision>.*)$', views.commitreport, name='report'),
    url(r'^report_dev/(?P<revision>.*)$', views.commitreport_dev, name='report_dev'),
    url(r'^list/author/(?P<author>.*)/$', views.list, name='list'),
    url(r'^(?P<number>.*)$', views.api_root),
])

# Login and logout views for the browsable API
urlpatterns += [url(r'^api-auth/$',
                    include('rest_framework.urls',
                            namespace='rest_framework')),
                ]
