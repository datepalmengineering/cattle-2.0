from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required
from reports import views

urlpatterns = patterns('', url(r'^$', login_required(views.index), name='index'), url(
    r'^(?P<reportid>\d+)$', login_required(views.reportview), name='reportview'), )
