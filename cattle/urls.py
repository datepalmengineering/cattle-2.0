from django.conf.urls import patterns, include, url
from django.contrib.auth.decorators import login_required
from django.contrib import admin
from buildreports import views
from commits.views import list
urlpatterns = patterns('',
                       url(r'^$', list, name='root'),
                       url(r'^branches/', include('branches.urls', namespace='branchesview')),
                       url(r'^commits/', include('commits.urls', namespace='commitsview')),
                       url(r'^buildreports/', include('buildreports.urls', namespace='buildreportsview')),
                       url(r'^sitreps/', include('sitreps.urls', namespace='sitrepsview')),
                       url(r'^runs/', include('runs.urls', namespace='runsview')),
                       url(r'^tests/', include('tests.urls', namespace='testsview')),
                       url(r'^admin/', include(admin.site.urls)),
                       url(r'^builders/', include('builders.urls', namespace='builders')),
                       url(r'^accounts/', include('accounts.urls', namespace='accounts')),
                       url(r'^logparser/', include('logparser.urls', namespace='logparser')),
                       )
