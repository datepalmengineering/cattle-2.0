from models import Run
from rest_framework import serializers
from django.contrib.auth.models import User


class RunSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Run
        fields = ('pk')
