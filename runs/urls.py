from django.conf.urls import patterns, url, include
from django.contrib.auth.decorators import login_required
from rest_framework.urlpatterns import format_suffix_patterns
from runs import views

urlpatterns = patterns('',
                       url(r'^rest/(?P<myrun>.*)$', views.api_root, name="rest"),
                       url(r'^$', views.index, name='index'),
                       url(r'^history/(?P<runid>\d+)/$', login_required(views.history), name='history'),
                       url(r'^bycommit/(?P<commit>\d+)/$', login_required(views.commitinfo), name='bycommit'),
                       url(r'^edit/(?P<runid>\d+)/$', login_required(views.editview), name='edit'),
                       url(r'^id/(?P<runid>\d+)/$', views.runinfo, name='runinfo'),
                       url(r'^builder/(?P<host>.*)/$', views.hostinfo, name='runinfo'),
                       url(r'^search/$', views.search_branch, name='search_branch'),
                       )

# Login and logout views for the browsable API
urlpatterns += [url(r'^api-auth/$',
                    include('rest_framework.urls',
                            namespace='rest_framework')),
                ]
