import time
from django.http import HttpResponse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render_to_response
from django.shortcuts import loader, render
from django.template import RequestContext
from django.http import HttpResponseRedirect
from rest_framework.decorators import api_view
from models import Run
from forms import RunEdit
from rest_framework.response import Response
from builders.models import Builder
from commits.models import Commit
from branches.models import Branch
from django.contrib.auth.models import User

ENTRIES_PER_PAGE = 100  # effects paginated views in this file

# Create your views here.


def index(request):
    run_list = Run.objects.all().order_by('-end_time')
    paginator = Paginator(run_list, ENTRIES_PER_PAGE)

    page = request.GET.get('page')
    try:
        runs = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        runs = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        runs = paginator.page(paginator.num_pages)

    return render_to_response(
        'runs/index.html', {"runs": runs, "request": request})


def search_branch(request):
    regex = request.GET.get('regex')
    run_list = Run.objects.all().filter(parent_branch__full__iregex=regex)
    paginator = Paginator(run_list, ENTRIES_PER_PAGE)

    page = request.GET.get('page')
    try:
        runs = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        runs = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        runs = paginator.page(paginator.num_pages)

    return render_to_response(
        'runs/index.html', {"runs": runs, "request": request})


def history(request, runid):
    run_list = Run.objects.filter(pk=runid)

    # if this is a POST request we need to process the form data
    message = ""

    restorepoint = request.POST.get('restorepoint')
    if restorepoint is not None:
        entry = Run.objects.all().get(pk=runid).history.all()
        if entry:
            for item in entry:
                if restorepoint in str(item):
                    run_list.update(
                        run_results=item.run_results,
                        for_bug=item.for_bug,
                        caused_bug=item.caused_bug,
                        comment=item.comment,
                        username=str(request.user),
                        entrytype="Restore"
                    )
                    run_list[0].save()
                    message += "Restoring to point: " + \
                        str(restorepoint) + " Potentially successful"
                    return HttpResponseRedirect(
                        request.META.get('HTTP_REFERER'))
        else:
            message += "Restore Failed"

    template = loader.get_template('runs/history.html')
    context = RequestContext(request, {
        "runs": run_list,
        "message": message,
    })
    return HttpResponse(template.render(context))


def editview(request, runid):
    run_list = Run.objects.filter(pk=runid)

    message = ""

    form = RunEdit(request.POST or None, instance=run_list[0])

    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        # now created before und stuff
        # check whether it's valid:
        if form.is_valid():
            adjusted_comment = form.cleaned_data['comment']
            if run_list[0].comment in adjusted_comment and run_list[
                    0].comment != adjusted_comment and run_list[0].comment:
                adjusted_comment = adjusted_comment.replace(
                    run_list[0].comment, run_list[0].comment + "\n~" + run_list[0].username, 1)
            run_list.update(
                run_results=form.cleaned_data['run_results'],
                caused_bug=form.cleaned_data['caused_bug'],
                comment=adjusted_comment,
                username=str(request.user),
                entrytype="Edit"
            )
            run_list[0].save()
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
        else:
            message = "invalid input, please adjust."

    template = loader.get_template('runs/edit.html')
    context = RequestContext(request, {
        "runs": run_list,
        "message": message,
        "form": form.as_p,
    })
    return HttpResponse(template.render(context))

# Create your views here.


def runinfo(request, runid):

    # try:
    run_list = Run.objects.filter(pk=runid)
    paginator = Paginator(run_list, ENTRIES_PER_PAGE)

    page = request.GET.get('page')
    try:
        runs = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        runs = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        runs = paginator.page(paginator.num_pages)

    return render_to_response(
        'runs/index.html', {"runs": runs.order_by('-end_time'), "request": request})
    # except:
    # return HttpResponse("You provided "+str(runid)+", which resulted in a
    # failure.<br /> Please ensure that you selected a valid run number.")


# Create your views here.
def hostinfo(request, host):

    # return HttpResponse(str(host))
    # try:
    run_list = Run.objects.filter(
        builder__address__contains=host.split("/")[-1]).order_by('-end_time')
    paginator = Paginator(run_list, ENTRIES_PER_PAGE)

    page = request.GET.get('page')
    try:
        runs = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        runs = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        runs = paginator.page(paginator.num_pages)

    return render_to_response(
        'runs/index.html', {"runs": runs, "request": request})


def commitinfo(request, commit):

    # try:
    run_list = Run.objects.filter(
        commit__revision=int(commit)).order_by('-end_time')
    paginator = Paginator(run_list, ENTRIES_PER_PAGE)

    page = request.GET.get('page')
    try:
        runs = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        runs = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        runs = paginator.page(paginator.num_pages)

    return render_to_response(
        'runs/index.html', {"runs": runs, "request": request})
    # except:
    # return HttpResponse("You provided "+str(runid)+", which resulted in a
    # failure.<br /> Please ensure that you selected a valid run number.")


@api_view(['GET', 'POST'])
def api_root(request, myrun):
    if request.method == 'POST':
        if 'pk' in request.data.keys():
            queryset = Run.objects.filter(pk=request.data["pk"])

            if queryset.exists():
                myrun = Run.objects.get(pk=request.data["pk"])[0]
                myrun.update(
                    run_results=request.data["results"],
                    caused_bug=request.data["caused_bug"],
                    comment=request.data["comment"],
                    username=str(request.user),
                    entrytype="REST Update"
                )
                myrun.save()
                return Response({"message": "Found a match, and updated!",
                                 "PK": myrun.pk, "pk": myrun.pk, "builder": str(myrun.builder)})
        else:

            mybuilder = Builder.objects.filter(address=request.data["builder"])
            if mybuilder.exists():
                mybuilder = mybuilder[0]
            else:
                mybuilder = Builder(address=request.data["builder"])
                mybuilder.save()

            mycommit = Commit.objects.filter(revision=request.data["revision"])
            if mycommit.exists():
                mycommit = mycommit[0]
            else:
                mycommit = Commit(revision=request.data["revision"])
                mycommit.save()

            branch_name = request.data["branch"]
            if branch_name.startswith('full/'):
                branch_name = branch_name[len('full/'):]

            mybranch = Branch.objects.filter(full=branch_name)
            if mybranch.exists():
                mybranch = mybranch[0]
            else:
                mybranch = Branch(full=branch_name)
                mybranch.save()

            if 'user' in request.data.keys():
                myUser = User.objects.filter(username=request.data['user'])
                if myUser.exists():
                    request.user = myUser[0]

            # check if entry exists
            queryset = Run.objects.filter(
                builder=mybuilder,
                build_number=request.data["build_number"])
            if queryset.exists():
                # update entry
                queryset.update(
                    builder=mybuilder,
                    commit=mycommit,
                    parent_branch=mybranch,
                    suite_file=request.data["suite"],
                    env_file=request.data["env"],
                    build_number=request.data["build_number"],
                    operating_system=request.data["os"],
                    run_results=request.data["results"],
                    caused_bug=request.data["bug"],
                    comment=request.data["comment"],
                    start_time=request.data["start"],
                    end_time=request.data["stop"],
                    username=str(request.user),
                    entrytype="REST Update"
                )
                queryset[0].save()

                if queryset.count() > 1:
                    queryset[1].delete()
                    return Response(
                        {
                            "message": "This run already exists, and had duplicates, but I fixed it.",
                            "PK": queryset[0].pk})

                return Response(
                    {"message": "This run already exists.", "PK": queryset[0].pk})

            else:
                # create new entry
                queryset = Run(
                    builder=mybuilder,
                    commit=mycommit,
                    parent_branch=mybranch,
                    suite_file=request.data["suite"],
                    env_file=request.data["env"],
                    build_number=request.data["build_number"],
                    operating_system=request.data["os"],
                    run_results=request.data["results"],
                    caused_bug=request.data["bug"],
                    comment=request.data["comment"],
                    start_time=request.data["start"],
                    end_time=request.data["stop"],
                    username=str(request.user),
                    entrytype="REST Create"
                )
                queryset.save()
                return Response(
                    {"message": "No match, new builder created.", "PK": queryset.pk})
    try:
        myrun.strip("/")
        int(myrun)
    except:
        myrun = 0

    queryset = Run.objects.filter(pk=myrun)
    if queryset.exists():
        return Response({"message": "Found a match!", "PK": queryset[
                        0].pk, "builder": str(queryset[0].builder)})
    else:
        return Response({"message": "No match found.", "run": str(myrun)})
