from django.forms import ModelForm, Textarea
from models import Run


class RunEdit(ModelForm):

    class Meta:
        model = Run
        fields = ['run_results', 'caused_bug', 'comment']
        widgets = {
            'comment': Textarea(attrs={'cols': 30, 'rows': 10}),
        }
