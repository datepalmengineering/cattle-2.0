from django.test import TestCase

from runs.models import Run
from builders.models import Builder
from commits.models import Commit
from branches.models import Branch


class RunTestCase(TestCase):

    def setUp(self):
        """
        This should make a run group.
        """
        mybuilder = Builder(address="test_builder")
        mybuilder.save()

        mycommit = Commit.objects.filter(revision="123")

        mycommit = Commit(revision=1234)
        mycommit.save()

        mybranch = Branch(full="/trunk")
        mybranch.save()

        Run.objects.create(
            builder=mybuilder,
            commit=mycommit,
            parent_branch=mybranch,
            suite_file="TEST/SUITE",
            env_file="myenv",
            build_number=222,
            operating_system="fake_os",
            run_results=1,
            caused_bug="",
            comment="No comment.",
            start_time="2000-01-01 01:01",
            end_time="2000-01-01 02:01",
            username="the user",
            entrytype="test create"
        )

    def test_created_run(self):
        """
        Basic search and verification of the created run.

        """
        myrun = Run.objects.get(build_number=222)
        self.assertEqual(getattr(myrun, "env_file"), "myenv")
        print ("env_file check passed.")
        self.assertEqual(getattr(myrun, "operating_system"), "fake_os")
        print ("operating_system check passed.")
        self.assertEqual(getattr(myrun, "run_results"), 1)
        print ("run_results check passed.")
        self.assertEqual(getattr(myrun, "comment"), "No comment.")
        print ("comment check passed.")
        self.assertEqual(getattr(myrun, "username"), "the user")
        print ("username check passed.")
        self.assertEqual(getattr(myrun, "suite_file"), "TEST/SUITE")
        print ("TEST/SUITE check passed.")
