from django.conf.urls import url, include
from django.conf.urls import include
from rest_framework.urlpatterns import format_suffix_patterns
from builders import views

# API endpoints
urlpatterns = format_suffix_patterns([
    url(r'^(?P<myaddress>.*)$', views.api_root),
])

# Login and logout views for the browsable API
urlpatterns += [url(r'^api-auth/$',
                    include('rest_framework.urls',
                            namespace='rest_framework')),
                ]
