from django.db import models

# Create your models here.


class Builder(models.Model):
    address = models.URLField(unique=True)

    class Meta:
        ordering = ('address',)

    def __str__(self):              # __unicode__ on Python 2
        return str(self.address.split('/')[-1])
