from models import Builder
from rest_framework import serializers
from django.contrib.auth.models import User


class BuilderSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Builder
        fields = ('address')
