from builders.models import Builder
from builders.serializers import BuilderSerializer
from rest_framework import generics

from django.contrib.auth.models import User
from models import Builder
from rest_framework import permissions
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework import renderers
from runs.models import Run


@api_view(['GET', 'POST'])
def api_root(request, myaddress):
    if request.method == 'POST':
        queryset = Builder.objects.filter(address=request.data["address"])

        if queryset.exists():
            latestrun = Run.objects.filter(
                builder=queryset[0]).order_by("-build_number")
            mynextbuild = 0
            if latestrun.exists():
                mynextbuild = latestrun[0].build_number + 1
            return Response({"message": "Found a match!", "PK": queryset[
                            0].pk, "address": queryset[0].address, "nextbuild": mynextbuild})
        else:
            queryset = Builder(address=request.data["address"])
            queryset.save()
            return Response(
                {"message": "No match, new builder created.", "PK": queryset.pk, "nextbuild": 0})

    queryset = Builder.objects.filter(address="http://" + str(myaddress))
    if queryset.exists():
        return Response({"message": "Found a match!", "PK": queryset[
                        0].pk, "address": queryset[0].address})
    else:
        return Response(
            {
                "message": "Post the builder address you need to add or search for.",
                "address": str(myaddress)})
