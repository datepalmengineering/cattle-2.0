from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required
import views


urlpatterns = patterns('', url(r'^$', views.index, name='index'), url(
    r'^view/(?P<buildreportid>\d+)/$', views.sitrep_viewmode, name='view_sitrep'), )
