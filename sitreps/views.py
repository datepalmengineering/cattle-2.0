import time
from django.http import HttpResponse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render_to_response
from django.shortcuts import loader, render
from django.template import RequestContext
from django.http import HttpResponseRedirect
import datetime
from buildreports.models import Buildreport
from buildreports.forms import BuildreportEdit
from runs.models import Run
from commits.models import Commit
from django.utils import timezone
from django.db.models import Q
from datetime import date, timedelta
ENTRIES_PER_PAGE = 10  # effects paginated views in this file

REPORTS_PER_PAGE = 20  # effects paginated views in this file


def index(request):
    return HttpResponse("Future home of sitrep list.")


def sitrep_viewmode(request, buildreportid):
    start = time.clock()

    Buildreport_list = Buildreport.objects.filter(pk=buildreportid)
    startdate = timezone.now() - datetime.timedelta(days=7)
    enddate = timezone.now()
    d = date.today() - timedelta(days=7)
    related_runs = Run.objects.filter(
        parent_branch=Buildreport_list[0].branch).filter(
        end_time__gt=d).exclude(
            run_results=11).exclude(
                run_results=12).exclude(
                    run_results=13).select_related()

    related_runtime = str(time.clock() - start)

    specific_runs = Run.objects.none()
    for mybuilder in Buildreport_list[0].builders.all():
        specific_runs = specific_runs | related_runs.filter(builder=mybuilder)

    related_runs = specific_runs.order_by('-end_time')

    specific_runtime = str(time.clock() - start - float(related_runtime))

    commitlist = related_runs.values("commit").distinct().order_by("-commit")

    suitelist = related_runs.values(
        "suite_file").distinct().order_by("suite_file")

    oslist = related_runs.values(
        "operating_system").distinct().order_by("operating_system")

    buglist = related_runs.values(
        "caused_bug").distinct().order_by("caused_bug")
    # given commit and suite list find last time run and status for each test
    # Format should look like this: {suite,lastruncommit,laststatus}

    lastRuns = []
    for mysuite in suitelist:
        myRun = related_runs.filter(
            suite_file=mysuite["suite_file"]).latest('end_time')
        lastRuns.append(myRun)

    totalSuites = 0
    totalPassing = 0
    totalFailing = 0
    totalInvalid = 0
    totalCurrentSuites = 0
    totalCurrentPassing = 0
    totalCurrentFailing = 0
    totalCurrentInvalid = 0

    for myRun in lastRuns:
        totalSuites += 1
        if myRun.run_results > 0 and myRun.run_results < 11:
            totalPassing += 1
            if myRun.commit.revision == commitlist[0]["commit"]:
                totalCurrentSuites += 1
                totalCurrentPassing += 1
        elif myRun.run_results == 0 or myRun.run_results > 20:
            totalFailing += 1
            if myRun.commit.revision == commitlist[0]["commit"]:
                totalCurrentSuites += 1
                totalCurrentFailing += 1

    commit_runtime = str(
        time.clock() -
        start -
        float(related_runtime) -
        float(specific_runtime))

    final_runs = Run.objects.none()
    commitstats = []
    for commit in commitlist:
        commitruns = specific_runs.filter(commit__revision=commit["commit"])
        passcount = 0
        failcount = 0
        runcount = 0
        for myRun in commitruns:
            if myRun.run_results > 0 and myRun.run_results < 11:
                passcount += 1
                runcount += 1
            elif myRun.run_results == 0 or myRun.run_results > 20:
                failcount += 1
                runcount += 1

        final_runs = final_runs | commitruns
        commitstats.append({"commit": commit[
                           "commit"], "passes": passcount, "fails": failcount, "runcount": runcount})

    final_runs = final_runs.order_by('-end_time')

    for suite in suitelist:
        if final_runs.filter(suite_file=suite["suite_file"]).count() == 0:
            suitelist = suitelist.exclude(suite_file=suite["suite_file"])
    template = loader.get_template('sitreps/sitrep_render.html')
    end = time.clock()
    context = RequestContext(
        request,
        {
            "commitlist": commitlist,
            "buildreports": Buildreport_list[0],
            "runs": final_runs,
            "commits": commitlist,
            "suitelist": suitelist,
            "request": request,
            "lastRuns": lastRuns,
            "totalSuites": totalSuites,
            "totalPassing": totalPassing,
            "totalFailing": totalFailing,
            "totalCurrentSuites": totalCurrentSuites,
            "totalCurrentPassing": totalCurrentPassing,
            "totalCurrentFailing": totalCurrentFailing,
            "buglist": buglist,
            "oslist": oslist,
            "commitstats": commitstats,
            "loadtime": "a total of " +
            str(
                end -
                start) +
            " Seconds, " +
            related_runtime +
            " Seconds of which was on pulling related runs, " +
            specific_runtime +
            " Seconds of which was on specific run calculations, then commit filtering took an additional " +
            commit_runtime,
        })
    return HttpResponse(template.render(context))
