from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required
import views


urlpatterns = patterns('',
                       url(r'^$', views.index, name='index'),
                       url(r'^all/$',
                           login_required(views.allview),
                           name='allview'),
                       url(r'^history/(?P<buildreportid>\d+)/$',
                           login_required(views.history), name='history'),
                       url(r'^edit/(?P<buildreportid>\d+)/$',
                           login_required(views.editview), name='edit'),
                       url(r'^view/(?P<buildreportid>\d+)/$',
                           views.viewmode, name='view'),
                       url(r'^view/(?P<buildreportid>\d+)/all/$',
                           views.viewmode_all, name='view_all'),
                       url(r'^view_sr/(?P<buildreportid>\d+)/$',
                           views.suitereport_viewmode, name='view'),
                       # url(r'^id/(?P<runid>\d+)/$', login_required(views.runinfo), name='runinfo'),
                       # url(r'^finished/after/(?P<date>.+)/$', login_required(views.searchafter), name='searchafter'),
                       # url(r'^bug/like/(?P<bugid>.+)/$', login_required(views.searchbug), name='searchbug'),
                       )
