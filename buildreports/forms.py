from django.forms import ModelForm, Textarea, SelectMultiple
from models import Buildreport


class BuildreportEdit(ModelForm):

    class Meta:
        model = Buildreport
        fields = [
            'title',
            'description',
            'private',
            'marked_for_removal',
            'branch',
            'builders']
        widgets = {
            'description': Textarea(attrs={'cols': 30, 'rows': 10}),
            'builders': SelectMultiple(),
        }
