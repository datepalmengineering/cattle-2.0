from django.db import models
from builders.models import Builder
from branches.models import Branch
from simple_history.models import HistoricalRecords
# Create your models here.


class Buildreport(models.Model):
    history = HistoricalRecords()
    title = models.CharField(max_length=128)
    description = models.CharField(max_length=512)
    branch = models.ForeignKey(Branch, null=True, blank=True)
    builders = models.ManyToManyField(Builder, null=True, blank=True)

    private = models.BooleanField(default=False)
    marked_for_removal = models.BooleanField(default=False)
    username = models.CharField(max_length=128, default="Automatic")
    entrytype = models.CharField(max_length=128, default="Automatic")

    def __str__(self):              # __unicode__ on Python 2
        return str(self.title)
