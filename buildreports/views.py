import time
from django.http import HttpResponse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render_to_response
from django.shortcuts import loader, render
from django.template import RequestContext
from django.http import HttpResponseRedirect

from models import Buildreport
from forms import BuildreportEdit
from runs.models import Run
from commits.models import Commit

ENTRIES_PER_PAGE = 10  # effects paginated views in this file

REPORTS_PER_PAGE = 20  # effects paginated views in this file

# Create your views here.


def index(request):
    public_list = Buildreport.objects.filter(
        private=False).filter(
        marked_for_removal=False)

    userreport_list = Buildreport.objects.filter(
        username=request.user).filter(
        private=True).filter(
            marked_for_removal=False)

    Buildreport_list = userreport_list | public_list

    Buildreport_list = Buildreport_list.order_by("-private")

    paginator = Paginator(Buildreport_list, REPORTS_PER_PAGE)

    if request.GET.get('makenew') == "true":
        newreport = Buildreport(
            title="new report",
            description="",
            private=False,
            username=str(request.user),
            entrytype="Creation"
        )
        newreport.save()
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

    page = request.GET.get('page')
    try:
        buildreports = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        buildreports = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        buildreports = paginator.page(paginator.num_pages)

    return render_to_response(
        'buildreports/index.html', {"buildreports": buildreports, "request": request})


def allview(request):
    Buildreport_list = Buildreport.objects.all()
    paginator = Paginator(Buildreport_list, REPORTS_PER_PAGE)

    page = request.GET.get('page')
    try:
        buildreports = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        buildreports = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        buildreports = paginator.page(paginator.num_pages)

    return render_to_response(
        'buildreports/index.html', {"buildreports": buildreports})


def history(request, buildreportid):
    Buildreport_list = Buildreport.objects.filter(pk=buildreportid)

    # if this is a POST request we need to process the form data
    message = ""

    restorepoint = request.POST.get('restorepoint')
    if restorepoint is not None:
        entry = Buildreport.objects.all().get(pk=buildreportid).history.all()
        if entry:
            for item in entry:
                if restorepoint in str(item):
                    Buildreport_list.update(
                        title=item.title,
                        description=item.description,
                        private=item.private,
                        username=str(request.user),
                        entrytype="Restore"
                    )
                    Buildreport_list[0].save()
                    message += "Restoring to point: " + \
                        str(restorepoint) + " Potentially successful"
                    return HttpResponseRedirect(
                        request.META.get('HTTP_REFERER'))
        else:
            message += "Restore Failed"

    template = loader.get_template('buildreports/history.html')
    context = RequestContext(request, {
        "buildreports": Buildreport_list,
        "message": message,
    })
    return HttpResponse(template.render(context))


def editview(request, buildreportid):
    Buildreport_list = Buildreport.objects.filter(pk=buildreportid)

    message = ""

    form = BuildreportEdit(request.POST or None, instance=Buildreport_list[0])

    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        # now created before und stuff
        # check whether it's valid:
        if form.is_valid():
            Buildreport_list.update(
                title=form.cleaned_data['title'],
                description=form.cleaned_data['description'],
                branch=form.cleaned_data['branch'],
                private=form.cleaned_data['private'],
                marked_for_removal=form.cleaned_data['marked_for_removal'],
                username=str(request.user),
                entrytype="Edit"
            )

            Buildreport_list[0].builders.clear()
            for builder in form.cleaned_data['builders']:
                Buildreport_list[0].builders.add(builder)

            Buildreport_list[0].save()
            # return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
            template = loader.get_template(
                'buildreports/redirect_template.html')
            context = RequestContext(request, {
                'redirect_url': "/buildreports/"
            })
            return HttpResponse(template.render(context))
        else:
            message = "invalid input, please adjust."

    template = loader.get_template('buildreports/edit.html')
    context = RequestContext(request, {
        "buildreport": Buildreport_list[0],
        "message": message,
        "form": form.as_p,
    })
    return HttpResponse(template.render(context))


def viewmode(request, buildreportid):
    start = time.clock()
    Buildreport_list = Buildreport.objects.filter(pk=buildreportid)

    related_runs = Run.objects.filter(
        parent_branch=Buildreport_list[0].branch).select_related()

    related_runtime = str(time.clock() - start)

    specific_runs = Run.objects.none()
    for mybuilder in Buildreport_list[0].builders.all():
        specific_runs = specific_runs | related_runs.filter(builder=mybuilder)
    related_runs = specific_runs.order_by('-end_time')

    specific_runtime = str(time.clock() - start - float(related_runtime))

    commitlist = specific_runs.values("commit").distinct().order_by("-commit")

    # commitlist=Commit.objects.all().order_by('-revision')

    # for mycommit in commitlist:
    #    if len(related_runs.filter(commit__pk = mycommit.pk)) == 0:
    #        commitlist = commitlist.exclude(revision = mycommit.revision)

    paginator = Paginator(commitlist, ENTRIES_PER_PAGE)

    commit_runtime = str(
        time.clock() -
        start -
        float(related_runtime) -
        float(specific_runtime))

    page = request.GET.get('page')
    try:
        commitlist = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        commitlist = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        commitlist = paginator.page(paginator.num_pages)

    final_runs = Run.objects.none()

    for commit in commitlist:
        final_runs = final_runs | specific_runs.filter(
            commit__revision=commit["commit"])

    final_runs = final_runs.order_by('-end_time')

    template = loader.get_template('buildreports/buildreport.html')
    end = time.clock()
    context = RequestContext(
        request,
        {
            "buildreports": Buildreport_list[0],
            "runs": final_runs,
            "commits": commitlist,
            "request": request,
            "loadtime": "a total of " +
            str(
                end -
                start) +
            " Seconds, " +
            related_runtime +
            " Seconds of which was on pulling related runs, " +
            specific_runtime +
            " Seconds of which was on specific run calculations, then commit filtering took an additional " +
            commit_runtime,
        })
    return HttpResponse(template.render(context))


def viewmode_all(request, buildreportid):
    start = time.clock()
    Buildreport_list = Buildreport.objects.filter(pk=buildreportid)

    related_runs = Run.objects.filter(
        parent_branch=Buildreport_list[0].branch).select_related()

    related_runtime = str(time.clock() - start)

    specific_runs = Run.objects.none()
    for mybuilder in Buildreport_list[0].builders.all():
        specific_runs = specific_runs | related_runs.filter(builder=mybuilder)
    related_runs = specific_runs.order_by('-end_time')

    specific_runtime = str(time.clock() - start - float(related_runtime))

    commitlist = specific_runs.values("commit").distinct().order_by("-commit")

    # commitlist=Commit.objects.all().order_by('-revision')

    # for mycommit in commitlist:
    #    if len(related_runs.filter(commit__pk = mycommit.pk)) == 0:
    #        commitlist = commitlist.exclude(revision = mycommit.revision)

    paginator = Paginator(commitlist, commitlist.count())

    commit_runtime = str(
        time.clock() -
        start -
        float(related_runtime) -
        float(specific_runtime))

    final_runs = Run.objects.none()

    for commit in commitlist:
        final_runs = final_runs | specific_runs.filter(
            commit__revision=commit["commit"])

    final_runs = final_runs.order_by('-end_time')

    template = loader.get_template('buildreports/buildreport.html')
    end = time.clock()
    context = RequestContext(
        request,
        {
            "buildreports": Buildreport_list[0],
            "runs": final_runs,
            "commits": commitlist,
            "request": request,
            "donotrefresh": True,
            "loadtime": "a total of " +
            str(
                end -
                start) +
            " Seconds, " +
            related_runtime +
            " Seconds of which was on pulling related runs, " +
            specific_runtime +
            " Seconds of which was on specific run calculations, then commit filtering took an additional " +
            commit_runtime,
        })
    return HttpResponse(template.render(context))


def suitereport_viewmode(request, buildreportid):
    start = time.clock()
    Buildreport_list = Buildreport.objects.filter(pk=buildreportid)

    related_runs = Run.objects.filter(
        parent_branch=Buildreport_list[0].branch).select_related()

    related_runtime = str(time.clock() - start)

    specific_runs = Run.objects.none()
    for mybuilder in Buildreport_list[0].builders.all():
        specific_runs = specific_runs | related_runs.filter(builder=mybuilder)
    related_runs = specific_runs.order_by('-end_time')

    specific_runtime = str(time.clock() - start - float(related_runtime))

    commitlist = specific_runs.values("commit").distinct().order_by("-commit")

    suitelist = specific_runs.values(
        "suite_file").distinct().order_by("suite_file")

    # commitlist=Commit.objects.all().order_by('-revision')

    # for mycommit in commitlist:
    #    if len(related_runs.filter(commit__pk = mycommit.pk)) == 0:
    #        commitlist = commitlist.exclude(revision = mycommit.revision)

    paginator = Paginator(commitlist, ENTRIES_PER_PAGE)

    commit_runtime = str(
        time.clock() -
        start -
        float(related_runtime) -
        float(specific_runtime))

    page = request.GET.get('page')
    try:
        commitlist = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        commitlist = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        commitlist = paginator.page(paginator.num_pages)

    final_runs = Run.objects.none()

    for commit in commitlist:
        final_runs = final_runs | specific_runs.filter(
            commit__revision=commit["commit"])

    final_runs = final_runs.order_by('-end_time')

    for suite in suitelist:
        if final_runs.filter(suite_file=suite["suite_file"]).count() == 0:
            suitelist = suitelist.exclude(suite_file=suite["suite_file"])
    template = loader.get_template('buildreports/suitereport.html')
    end = time.clock()
    context = RequestContext(
        request,
        {
            "buildreports": Buildreport_list[0],
            "runs": final_runs,
            "commits": commitlist,
            "suitelist": suitelist,
            "request": request,
            "loadtime": "a total of " +
            str(
                end -
                start) +
            " Seconds, " +
            related_runtime +
            " Seconds of which was on pulling related runs, " +
            specific_runtime +
            " Seconds of which was on specific run calculations, then commit filtering took an additional " +
            commit_runtime,
        })
    return HttpResponse(template.render(context))
