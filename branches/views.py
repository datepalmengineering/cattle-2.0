import time
from django.http import HttpResponse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render_to_response
from django.shortcuts import loader, render
from django.template import RequestContext
from django.http import HttpResponseRedirect

from models import Branch
from forms import RunEdit


ENTRIES_PER_PAGE = 5  # effects paginated views in this file

# Create your views here.


def index(request):
    branch_list = Branch.objects.all()
    paginator = Paginator(branch_list, ENTRIES_PER_PAGE)

    page = request.GET.get('page')
    try:
        branches = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        branches = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        branches = paginator.page(paginator.num_pages)

    return render_to_response('branches/index.html', {"branches": branches})


def history(request, branchid):
    branch_list = Branch.objects.filter(pk=branchid)

    # if this is a POST request we need to process the form data
    message = ""

    restorepoint = request.POST.get('restorepoint')
    if restorepoint is not None:
        entry = Branch.objects.all().get(pk=branchid).history.all()
        if entry:
            for item in entry:
                if restorepoint in str(item):
                    branch_list.update(
                        name=item.name,
                        full=item.full,
                        summary=item.summary,
                        comment=item.comment,
                        username=str(request.user),
                        entrytype="Restore"
                    )
                    branch_list[0].save()
                    message += "Restoring to point: " + \
                        str(restorepoint) + " Potentially successful"
                    return HttpResponseRedirect(
                        request.META.get('HTTP_REFERER'))
        else:
            message += "Restore Failed"

    template = loader.get_template('branches/history.html')
    context = RequestContext(request, {
        "branches": branch_list,
        "message": message,
    })
    return HttpResponse(template.render(context))


def editview(request, branchid):
    branch_list = Branch.objects.filter(pk=branchid)

    message = ""

    form = RunEdit(request.POST or None, instance=branch_list[0])

    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        # now created before und stuff
        # check whether it's valid:
        if form.is_valid():
            branch_list.update(
                summary=form.cleaned_data['summary'],
                comment=form.cleaned_data['comment'],
                username=str(request.user),
                entrytype="Edit"
            )
            branch_list[0].save()
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
        else:
            message = "invalid input, please adjust."

    template = loader.get_template('branches/edit.html')
    context = RequestContext(request, {
        "branch": branch_list[0],
        "message": message,
        "form": form.as_p,
    })
    return HttpResponse(template.render(context))
