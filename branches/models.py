import datetime
from django.utils import timezone
from django.db import models
from simple_history.models import HistoricalRecords
from builders.models import Builder

# Create your models here.


class Branch(models.Model):
    history = HistoricalRecords()

    # run information
    name = models.CharField(max_length=128)
    full = models.CharField(max_length=255, unique=True)
    summary = models.CharField(max_length=1024, blank=True)
    comment = models.CharField(max_length=1024, blank=True)

    last_update = models.DateTimeField(
        'Entry updated at', default=timezone.now)
    username = models.CharField(max_length=128, default="Automatic")
    entrytype = models.CharField(max_length=128, default="Automatic")

    def __str__(self):              # __unicode__ on Python 2
        return str(self.full)


class Filter(models.Model):
    history = HistoricalRecords()
    # run information
    name = models.CharField(max_length=128, unique=True)
    description = models.CharField(max_length=512)
    regmatch = models.CharField(max_length=512)

    last_update = models.DateTimeField(
        'Entry updated at', default=timezone.now)
    username = models.CharField(max_length=128, default="Automatic")
    entrytype = models.CharField(max_length=128, default="Automatic")

    def __str__(self):              # __unicode__ on Python 2
        return str(self.name) + " => " + str(self.regmatch)


class Criterium(models.Model):
    # run information
    name = models.CharField(max_length=128, unique=True)
    description = models.CharField(max_length=512)
    filter = models.ForeignKey(Filter)
    builders = models.ManyToManyField(Builder, null=True, blank=True)
    suitematch = models.CharField(max_length=128, null=True, blank=True)
    optional = models.BooleanField(default=False)
    compiler = models.BooleanField(default=False)
    must_pass = models.BooleanField(default=True)

    last_update = models.DateTimeField(
        'Entry updated at', default=timezone.now)
    username = models.CharField(max_length=128, default="Automatic")
    entrytype = models.CharField(max_length=128, default="Automatic")

    def __str__(self):              # __unicode__ on Python 2
        return str(self.filter.name) + " => " + \
            str(self.name) + " => " + str(self.suitematch)
