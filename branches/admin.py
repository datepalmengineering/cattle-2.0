from django.contrib import admin
from models import Branch, Filter, Criterium

admin.site.register(Branch)
admin.site.register(Filter)
admin.site.register(Criterium)
