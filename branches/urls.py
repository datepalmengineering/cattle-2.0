from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required
from branches import views


urlpatterns = patterns('',
                       url(r'^$', login_required(views.index), name='index'),
                       url(r'^history/(?P<branchid>\d+)/$',
                           login_required(views.history), name='history'),
                       url(r'^edit/(?P<branchid>\d+)/$',
                           login_required(views.editview), name='edit'),
                       # url(r'^id/(?P<runid>\d+)/$', login_required(views.runinfo), name='runinfo'),
                       # url(r'^finished/after/(?P<date>.+)/$', login_required(views.searchafter), name='searchafter'),
                       # url(r'^bug/like/(?P<bugid>.+)/$', login_required(views.searchbug), name='searchbug'),
                       )
