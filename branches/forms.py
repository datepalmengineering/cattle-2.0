from django.forms import ModelForm, Textarea
from models import Branch


class RunEdit(ModelForm):

    class Meta:
        model = Branch
        fields = ['summary', 'comment']
        widgets = {
            'comment': Textarea(attrs={'cols': 30, 'rows': 10}),
        }
