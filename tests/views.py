import time
from django.http import HttpResponse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render_to_response
from django.shortcuts import loader, render
from django.template import RequestContext
from django.http import HttpResponseRedirect
from rest_framework.response import Response
from models import Test
from runs.models import Run
from forms import TestEdit
from rest_framework.decorators import api_view
from builders.models import Builder


ENTRIES_PER_PAGE = 100  # effects paginated views in this file

# Create your views here.


def index(request):
    test_list = Test.objects.all().order_by('-last_update')
    paginator = Paginator(test_list, ENTRIES_PER_PAGE)

    page = request.GET.get('page')
    try:
        tests = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        tests = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        tests = paginator.page(paginator.num_pages)

    return render_to_response(
        'tests/index.html', {"tests": tests, "request": request})

# Create your views here.


def searchafter(request, date):

    try:
        test_list = Test.objects.filter(
            creation_date__gte=str(date) +
            " 00:00").order_by('-last_update')
        paginator = Paginator(test_list, ENTRIES_PER_PAGE)

        page = request.GET.get('page')
        try:
            tests = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            tests = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of
            # results.
            tests = paginator.page(paginator.num_pages)

        return render_to_response(
            'tests/index.html', {"tests": tests, "request": request})
    except:
        return HttpResponse(
            "You provided " +
            str(date) +
            ", which resulted in a failure.<br /> Please use date format yyyy-mm-dd, and make sure to include the dashes.")


# Create your views here.
def searchbug(request, bugid):

    try:
        test_list = Test.objects.filter(
            caused_bug__contains=str(bugid)).order_by('-last_update')
        paginator = Paginator(test_list, ENTRIES_PER_PAGE)

        page = request.GET.get('page')
        try:
            tests = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            tests = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of
            # results.
            tests = paginator.page(paginator.num_pages)

        return render_to_response(
            'tests/index.html', {"tests": tests, "request": request})
    except:
        return HttpResponse(
            "You provided " +
            str(bugid) +
            ", which resulted in a failure.<br /> Please ensure that you selected a valid JIRA issue that has been logged in a test. Format is generally &&&-####.")


def history(request, testid):
    test_list = Test.objects.filter(pk=testid)

    # if this is a POST request we need to process the form data
    message = ""

    restorepoint = request.POST.get('restorepoint')
    if restorepoint is not None:
        entry = Test.objects.all().get(pk=testid).history.all()
        if entry:
            for item in entry:
                if restorepoint in str(item):
                    test_list.update(
                        test_conditions=item.test_conditions,
                        test_args=item.test_args,
                        test_sequence_number=item.test_sequence_number,
                        data_file=item.data_file,
                        test_results=item.test_results,
                        caused_bug=item.caused_bug,
                        comment=item.comment,
                        test_duration=item.test_duration,
                        last_update=item.last_update,
                        username=str(request.user),
                        entrytype="Restore"
                    )
                    test_list[0].save()
                    message += "Restoring to point: " + \
                        str(restorepoint) + " Potentially successful"
                    return HttpResponseRedirect(
                        request.META.get('HTTP_REFERER'))
        else:
            message += "Restore Failed"

    template = loader.get_template('tests/history.html')
    context = RequestContext(request, {
        "tests": test_list,
        "message": message,
    })
    return HttpResponse(template.render(context))


def editview(request, testid):
    test_list = Test.objects.filter(pk=testid)

    message = ""

    form = TestEdit(request.POST or None, instance=test_list[0])

    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        # now created before und stuff
        # check whether it's valid:
        if form.is_valid():
            adjusted_comment = form.cleaned_data['comment']
            if test_list[0].comment in adjusted_comment and test_list[
                    0].comment != adjusted_comment and test_list[0].comment:
                adjusted_comment = adjusted_comment.replace(
                    test_list[0].comment, test_list[0].comment + "\n~" + test_list[0].username, 1)
            test_list.update(
                test_results=form.cleaned_data['test_results'],
                caused_bug=form.cleaned_data['caused_bug'],
                comment=adjusted_comment,
                username=str(request.user),
                entrytype="Edit"
            )
            test_list[0].save()
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
        else:
            message = "invalid input, please adjust."

    template = loader.get_template('tests/edit.html')
    context = RequestContext(request, {
        "tests": test_list,
        "message": message,
        "form": form.as_p,
    })
    return HttpResponse(template.render(context))


def rungroup(request, runid):
    test_list = Test.objects.filter(parentrun=runid).order_by('-last_update')
    paginator = Paginator(test_list, ENTRIES_PER_PAGE)  # Show 5 tests per page

    page = request.GET.get('page')
    try:
        tests = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        tests = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        tests = paginator.page(paginator.num_pages)

    return render_to_response(
        'tests/index.html', {"tests": tests, "request": request})


@api_view(['GET', 'POST'])
def api_root(request, testid):
    if request.method == 'POST':
        if 'pk' in request.data.keys():
            queryset = Test.objects.filter(pk=request.data["pk"])

            if queryset.exists():
                testid = Test.objects.get(pk=request.data["pk"])[0]
                testid.update(
                    test_results=request.data["results"],
                    caused_bug=request.data["caused_bug"],
                    comment=request.data["comment"],
                    username=str(request.user),
                    entrytype="REST Update"
                )
                testid.save()
                return Response(
                    {
                        "message": "Found a match, and updated!",
                        "PK": testid.pk,
                        "pk": testid.pk,
                        "builder": str(
                            testid.builder)})
        else:
            mybuilder = Builder.objects.filter(address=request.data["builder"])
            if mybuilder.exists():
                mybuilder = mybuilder[0]
            else:
                return Response(
                    {"message": "No run to associate to, builder not found"})

            myrun = Run.objects.filter(
                builder=mybuilder, build_number=int(
                    request.data["build_number"]))
            if myrun.exists():
                myrun = myrun[0]
            else:
                return Response({"message": "No run to associate to."})

            # check if entry exists
            queryset = Test.objects.filter(
                parentrun=myrun, test_sequence_number=request.data["sequence"])
            if queryset.exists():
                # update entry
                queryset.update(
                    parentrun=myrun,
                    test_conditions=request.data["conditions"],
                    test_args=request.data["args"],
                    test_sequence_number=request.data["sequence"],
                    data_file=request.data["data_file"],
                    test_results=request.data["results"],
                    caused_bug=request.data["bug"],
                    comment=request.data["comment"],
                    test_duration=request.data["duration"],
                    username=str(request.user),
                    entrytype="REST Update"
                )
                queryset[0].save()

                return Response(
                    {"message": "This test already exists, updating.", "PK": queryset[0].pk})

            else:
                # create new entry
                queryset = Test(
                    parentrun=myrun,
                    test_conditions=request.data["conditions"],
                    test_args=request.data["args"],
                    test_sequence_number=request.data["sequence"],
                    data_file=request.data["data_file"],
                    test_results=request.data["results"],
                    caused_bug=request.data["bug"],
                    comment=request.data["comment"],
                    test_duration=request.data["duration"],
                    username=str(request.user),
                    entrytype="REST Create"
                )
                queryset.save()
                return Response(
                    {"message": "No match, new test entry created.", "PK": queryset.pk})
    try:
        testid.strip("/")
        int(testid)
    except:
        testid = 0

    queryset = Test.objects.filter(pk=testid)
    if queryset.exists():
        return Response({"message": "Found a match!", "PK": queryset[
                        0].pk, "builder": str(queryset[0].builder)})
    else:
        return Response({"message": "No match found.", "run": str(testid)})
