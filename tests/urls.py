from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required
import views

urlpatterns = patterns('',
                       url(r'^$', views.index, name='index'),
                       url(r'^rest/(?P<testid>.*)$', views.api_root, name="rest"),
                       url(r'^run/(?P<runid>\d+)/$', views.rungroup, name='rungroup'),
                       url(r'^history/(?P<testid>\d+)/$', login_required(views.history), name='history'),
                       url(r'^edit/(?P<testid>\d+)/$', login_required(views.editview), name='edit'),
                       url(r'^finished/after/(?P<date>.+)/$', views.searchafter, name='searchafter'),
                       url(r'^bug/like/(?P<bugid>.+)/$', views.searchbug, name='searchbug'),
                       )
