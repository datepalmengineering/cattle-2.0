from django.test import TestCase

from .models import Test
from runs.models import Run
from builders.models import Builder
from commits.models import Commit
from branches.models import Branch


class TestTestCase(TestCase):

    def setUp(self):
        """
        This should make a run group.
        """
        mybuilder = Builder(address="test_builder")
        mybuilder.save()

        mycommit = Commit.objects.filter(revision="123")

        mycommit = Commit(revision=1234)
        mycommit.save()

        mybranch = Branch(full="/trunk")
        mybranch.save()

        myRun = Run(
            builder=mybuilder,
            commit=mycommit,
            parent_branch=mybranch,
            suite_file="TEST/SUITE",
            env_file="myenv",
            build_number=222,
            operating_system="fake_os",
            run_results=1,
            caused_bug="",
            comment="No comment.",
            start_time="2000-01-01 01:01",
            end_time="2000-01-01 02:01",
            username="the user",
            entrytype="run create"
        )
        myRun.save()

        myTest = Test(
            parentrun=myRun,
            test_conditions="Amazing conditions",
            test_args="Test arguments",
            test_sequence_number=1,
            data_file="/opt/datafile",
            test_results=1,
            caused_bug="",
            comment="No comment",
            test_duration=60,
            username="the user",
            entrytype="test create"
        )
        myTest.save()

    def test_created_tests(self):
        """
        Basic search and verification of the created run.

        """
        myrun = Run.objects.get(build_number=222)
        mytest = Test.objects.get(parentrun=myrun)

        self.assertEqual(
            getattr(
                mytest,
                "test_conditions"),
            "Amazing conditions")
        print ("test_conditions check passed.")
        self.assertEqual(getattr(mytest, "test_args"), "Test arguments")
        print ("test_args check passed.")
        self.assertEqual(getattr(mytest, "test_sequence_number"), 1)
        print ("test_sequence_number check passed.")
        self.assertEqual(getattr(mytest, "comment"), "No comment")
        print ("comment check passed.")
        self.assertEqual(getattr(mytest, "username"), "the user")
        print ("username check passed.")
        self.assertEqual(getattr(mytest, "test_duration"), 60)
        print ("test_duration check passed.")
