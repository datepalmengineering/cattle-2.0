from django.forms import ModelForm, Textarea
from models import Test


class TestEdit(ModelForm):

    class Meta:
        model = Test
        fields = ['test_results', 'caused_bug', 'comment']
        widgets = {
            'comment': Textarea(attrs={'cols': 30, 'rows': 10}),
        }
