from builders.models import Builder
from builders.serializers import BuilderSerializer
from rest_framework import generics

from django.contrib.auth.models import User
from rest_framework import permissions
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework import renderers
from parserfunctions import parselog


@api_view(['GET', 'POST'])
def api_root(request, myaddress):
    if request.method == 'POST':
        return Response({"message": parselog(
            request.data["builder"], request.data["buildnum"], request.data["env"])})
    else:
        return Response(
            {"message": "You need to post a builder, buildnum, suite, and env at a minimum"})
