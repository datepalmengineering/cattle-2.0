import json
import requests
import urllib
import time
from datetime import datetime, date
import traceback

# This is used by getbuilderstate


def getbuildbotproperty(myproperties, myfield):
    """Function to retrieve build property from buildbot JSON pulls"""
    myreturn = "NA"
    for prop in myproperties:
        try:
            if(prop[0] == myfield):
                myreturn = prop[1]
                break
        except Exception as e:
            myreturn = "<!-- " + str(e) + " -->"
    return myreturn

# microtime function for calculations of time difference


def microtime(get_as_float=False):
    """Convert float to microtime"""
    if get_as_float:
        return time.time()
    else:
        return '%f %d' % math.modf(time.time())


def parselog(builder, buildnum, env):
    """Performs log parsing operations"""
    jsonbuildaddress = str(builder).split("builders")[0] + "json/builders" + str(
        builder).split("builders")[1] + "/builds/" + str(buildnum) + "?as_text=1"
    builderreport = ""
    try:
        loadedjson = urllib.urlopen(jsonbuildaddress).read()
        result = json.loads(loadedjson)
        parent_branch = result['sourceStamps'][0]['branch']
        commit = getbuildbotproperty(result['properties'], "got_revision")
        suite_file = getbuildbotproperty(result['properties'], "suite_files")
        env_file = str(env)
        operating_system = getbuildbotproperty(result['properties'], "os")
        builder = builder
        build_number = int(buildnum)

        # #####CALCULATE RESULT CODE#######
        results = 1
        resultlist = []
        for value in result['text']:
            resultlist.append(value)
        try:
            if "failed" in resultlist:
                results = 0
            elif "successful" in resultlist or "passed" in resultlist:
                results = 1
            else:
                results = 12
        except Exception as e:
            print str(traceback.format_exc())

        start_time = ""
        end_time = ""

        # ######################################TIMES###########################
        if result['times'][1] is not None:
            start_time = str(result['times'][0])
            end_time = str(result['times'][1])
            # builderreport = builderreport + ", Duration " + str(int(result['times'][1]-result['times'][0]))
        else:
            start_time = str(result['times'][0])
            endtime = microtime(True)
            end_time = str(endtime)
            # builderreport = builderreport + ', Duration: '
            # builderreport = builderreport + str(int(endTime-result['times'][0]))

        # #################################CREATE TEST ENTRIES##################
        # ----------Open log_file
        _parsedsuitefile = "UNAVAILABLE"
        _parsedenvfile = "UNAVAILABLE"

        try:
            fulltxt = urllib.urlopen(
                builder +
                "/builds/" +
                str(build_number) +
                "/steps/test/logs/ae2_debug/text").read()
            # fix problem with documents using different return types.
            fulltxt = fulltxt.replace('\r', '\n')

            currentcut = fulltxt

            # #####################################SUITE_FILE PARSINATION#######
            suite_file = str(suite_file)
            if suite_file != "UNAVAILABLE" and suite_file != "NA" and suite_file != "":
                _parsedsuitefile = suite_file
            else:
                try:
                    currentcut = currentcut.split("suite_files")
                    currentcut = currentcut[1].split(" ")
                    currentcut = "suite_files" + currentcut[0]
                    _parsedsuitefile = currentcut
                except Exception as e:
                    print (
                        "Unable to parse Suite File: \n" +
                        traceback.format_exc())

            currentcut = fulltxt
            # #####################################ENV_FILE PARSINATION#########

            env_file = str(env_file)
            if env_file != "":
                _parsedenvfile = env_file
            else:
                try:
                    currentcut = currentcut.split("env_files")
                    currentcut = currentcut[1].split(" ")
                    currentcut = currentcut[0]
                    _parsedenvfile = "env_files" + currentcut
                except Exception as e:
                    print (
                        "Unable to parse ENV FILE: \n" +
                        traceback.format_exc())

        except Exception as e:
            return ("FAIL: NO LOG FILE ->" + str(e))

        # ###################################CREATE RUN ENTRY###################
        cattle2_url = "http://cattle.yourdomain.com"
        data = json.dumps({
            "builder": str(builder),
            "revision": commit,
            "branch": str(parent_branch),
            "suite": str(_parsedsuitefile),
            "env": str(_parsedenvfile),
            "build_number": build_number,
            "os": operating_system,
            "log_file": str(builder) + "/builds/" + str(build_number) + "/steps/test/logs/ae2",
            "results": results,
            "bug": "",
            "comment": "",
            "start": datetime.fromtimestamp(float(start_time)).strftime("%Y-%m-%d %H:%M:%S"),
            "stop": datetime.fromtimestamp(float(end_time)).strftime("%Y-%m-%d %H:%M:%S")
        })
        headers = {'content-type': 'application/json'}
        r = requests.post(
            cattle2_url +
            "/runs/rest/",
            data,
            auth=(
                'root',
                'cleverpassword'),
            headers=headers)

        # #################################CREATE TEST ENTRIES##################
        # ----------Open log_file
        try:
            fulltxt = urllib.urlopen(
                builder +
                "/builds/" +
                str(build_number) +
                "/steps/test/logs/ae2/text").read()
        except Exception as e:
            return ("FAIL: NO LOG FILE ->" + str(e))
        # fix problem with documents using different return types.
        fulltxt = fulltxt.replace('\r', '\n')

        AESPLIT = fulltxt.split("---   AE FINISHED   ---")
        for txt in AESPLIT:
            if txt == '\n' or txt == '':
                break
            # ------------------DIVE INTO TEST EXECUTION SUMMARY
            try:
                # print("############PULLING OUT DATA FROM TEST EXECUTION
                # SUMMARY")
                currentcut = txt.split(
                    "TEST EXECUTION SUMMARY\n-----------------\n\n")
                if len(currentcut) > 1:
                    currentcut = currentcut[1].split("\nFINAL RESULTS")
                    currentcut = currentcut[0].split("<TESTBLOCK>\n")
                else:
                    currentcut = currentcut[0].split("\nFINAL RESULTS")
                    currentcut = currentcut[0].split("<TESTBLOCK>\n")
                if len(currentcut[0]) > 1:
                    while currentcut[0][0] == '\n' and len(currentcut[0]) > 1:
                        currentcut[0] = currentcut[0][1:]
                parsedTestList = currentcut
                # return parsedTestList
                # this list will be further parsed during the logging phase below.
                # print ("!********* FINAL Test List has "+str(len(parsedTestList))+" members.")

                # ----------current result number

                # ################################BEGIN PARSAMAJIGGER###########
                # ##############################################################
                myCount = 0
                for testCases in parsedTestList:
                    _testconditions = testCases.split("\n")[0]
                    testCases = testCases.split("<ENDTEST>")
                    for completedTest in testCases:
                        if len(completedTest) < 50:
                            continue
                        myCount = myCount + 1
                        _testargs = completedTest.split(
                            "Args:")[1].split('\n')[0]
                        _testargs = _testargs.split(',')
                        _testargs = ','.join(
                            [x for x in _testargs if 'rpm' not in x])
                        _testargs = _testargs.translate(
                            None,
                            ' ').translate(
                            None,
                            '}').translate(
                            None,
                            '{').translate(
                            None,
                            "'")
                        _testresults = completedTest.split(
                            "RESULT:")[1].split('\n')[0]
                        if "PASSED" not in _testresults:
                            _testresults = "0"
                        else:
                            _testresults = "1"
                        _datafile_url = "N/A"
                        if "Data File:" not in completedTest:
                            _datafile_url = ""
                        else:
                            _datafile_url = completedTest.split(
                                "Data File:")[1].split("\n")[0]
                        _testduration = completedTest.split(
                            "DURATION:")[1].split('\n')[0].split(':')
                        _testduration = int(
                            _testduration[0]) * 3600 + int(_testduration[1]) * 60 + int(_testduration[2])
                        # ##################Make Test creation REST calls#######

                        cattle2_url = "http://cattle.yourdomain.com"
                        data = json.dumps({
                            "builder": str(builder),
                            "build_number": build_number,
                            "conditions": _testconditions,
                            "args": _testargs,
                            "sequence": myCount,
                            "data_file": _datafile_url,
                            "results": _testresults,
                            "bug": "",
                            "duration": str(_testduration),
                            "comment": ""
                        })
                        headers = {'content-type': 'application/json'}
                        r = requests.post(
                            cattle2_url +
                            "/tests/rest/",
                            data,
                            auth=(
                                'root',
                                'cleverpassword'),
                            headers=headers)

                        # ------------------JUMP BACK TO LOOP START IF MORE ENTR
                # #################################END PARSAMAJIGGER############
            except Exception as e:
                return "FAIL: LOG PARSE ERROR -> " + \
                    str(e) + " " + str(traceback.format_exc())
        return str(r.json)
    except Exception as e:
        # decoding failed
        builderreport = "Failed to parse/load JSON " + \
            str(e) + " " + str(traceback.format_exc())
    return str(builderreport)
